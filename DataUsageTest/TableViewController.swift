//
//  TableViewController.swift
//  DataUsageTest
//
//  Created by meysam on 8/7/1396 AP.
//
//

import UIKit

class TableViewController: UITableViewController {

    var packageSize = "0"
    var usedData = "0"
    var npUsage = "0"
    var currentUsage = "0"

    
    @IBOutlet var dataUsedLbl: UILabel!
    @IBOutlet var remainLbl: UILabel!
    @IBOutlet var pakageSizeLbl: UILabel!
    
    var defaults : UserDefaults!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        defaults = UserDefaults(suiteName: "group.dataUsage")

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//         self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        
        
    }
    

    
//    func valueChangedChanged(type: DataType, value: String){
//        
//        
//        if value != "" {
//
//        switch type {
//        case .PackageSize:
//            
//            
//            packageSize = value
//
//            defaults?.set(packageSize, forKey: "packageSize")
//            defaults?.synchronize()
//            
//            
//        case .UsedData:
//            
//            usedData = value
//        }
//        
//        updateData()
//            
//        }
//
//    }
//    
    func updateData() {
        
        if let packageSize2:String = defaults?.object(forKey: "packageSize") as? String
        {
            packageSize = packageSize2
        }
        
        if let usedData2:String = defaults?.object(forKey: "usedData") as? String
        {
            usedData = usedData2
        }
        
        if let npUsage2:String = defaults?.object(forKey: "npUsage") as? String
        {
            
        } else {
            
            npUsage = String (DataUsage.getDataUsage().wifiReceived / 1024 / 1024 )
            
            defaults.set(npUsage, forKey: "npUsage")
        }
        
        currentUsage = String( (DataUsage.getDataUsage().wifiReceived / 1024 / 1024) - UInt32(npUsage)! )
        
        defaults.set(currentUsage, forKey: "currentUsage")

        pakageSizeLbl.text = packageSize + "MB"
        dataUsedLbl.text = usedData + "MB"
        
        if Int(packageSize)! - Int(usedData)! < 0 {
            
            remainLbl.text = "0" + "MB"
            
        } else {
            
            remainLbl.text = String( Int(packageSize)! - Int(usedData)! ) + "MB"
        }
    }

        
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
//        guard let segueId = segue.identifier else { return }
//        
//        switch segueId {
//            
//        case "packageSize":
//            
//            let destVC = segue.destination as! PackageSizeViewController
//            
//            destVC.delegate = self
//            
//        case "UsedData":
//            
//            let destVC = segue.destination as! UsedDataViewController
//                destVC.delegate = self
//            
//        default:
//            break
//        }
//        print(segue)
//        print(sender ?? "segue")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        updateData()
    }
    

}
