//
//  PackageSizeViewController.swift
//  DataUsageTest
//
//  Created by meysam on 8/7/1396 AP.
//
//

import UIKit

class PackageSizeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var unitLbl: UILabel!
    @IBOutlet var unitPicker: UIPickerView!
    @IBOutlet var packageSizeTF: UITextField!
    var defaults : UserDefaults!
    
    var dataUnit = DataUnit()

    override func viewDidLoad() {
        super.viewDidLoad()

        defaults = UserDefaults(suiteName: "group.dataUsage")
        
        packageSizeTF.becomeFirstResponder()
        
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return 2
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if row == 0 {
            
            return "MB"
        } else {
            
            return "GB"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if row == 0 {
            
            unitLbl.text = "MB"
            
        } else {
            
            unitLbl.text = "GB"

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if packageSizeTF.text != "" {
            
            defaults?.set(packageSizeTF.text, forKey: "packageSize")
            defaults?.synchronize()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let packageSize2:String = defaults?.object(forKey: "packageSize") as? String
        {
            packageSizeTF.text = packageSize2
        }
    }
    
    
        // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }
    

}
