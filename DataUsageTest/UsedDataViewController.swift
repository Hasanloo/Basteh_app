//
//  UsedDataViewController.swift
//  DataUsageTest
//
//  Created by meysam on 8/8/1396 AP.
//
//

import UIKit

class UsedDataViewController: UIViewController  {

    @IBOutlet var usedDataTF: UITextField!
    var defaults : UserDefaults!


    override func viewDidLoad() {
        super.viewDidLoad()

        defaults = UserDefaults(suiteName: "group.dataUsage")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if usedDataTF.text != "" {
            
            defaults?.set(usedDataTF.text, forKey: "usedData")
            defaults?.synchronize()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let usedData2:String = defaults?.object(forKey: "usedData") as? String
        {
            usedDataTF.text = usedData2
        }
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
