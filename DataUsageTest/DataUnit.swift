//
//  DataUnit.swift
//  DataUsageTest
//
//  Created by meysam on 8/9/1396 AP.
//
//

import UIKit

struct DataUnit {
    
    var value: Int!
    var unit: _Unit!
    
    mutating func toMB() -> DataUnit {

        value = value * 1000
        unit = .MB
        return self
    }
    
    mutating func toGB() -> DataUnit {
        
        value = value / 1000
        unit = .GB
        return self
    }
}
