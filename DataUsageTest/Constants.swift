//
//  Constants.swift
//  DataUsageTest
//
//  Created by meysam on 8/8/1396 AP.
//
//

import Foundation

enum DataType{
    case PackageSize
    case UsedData
    
}

enum _Unit {
    
    case MB
    case GB
}
