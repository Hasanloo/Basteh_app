//
//  TodayViewController.swift
//  DataUsageWidget
//
//  Created by meysam on 8/7/1396 AP.
//
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet var myLabel: UILabel!
    
    var packageSize = "0"
    var totalUsedData = "0"
    var currentUsage = "0"
    var usedData = "0"
    var npUsage = "0"
    var percent = "0%"
    var defaults : UserDefaults!
    
    typealias mb = Int
    
    @IBOutlet var percentLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        defaults = UserDefaults(suiteName: "group.dataUsage")

    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        
        if let npUsage2:String = defaults?.object(forKey: "npUsage") as? String
        {
            npUsage = npUsage2
            
        } else {
            
            npUsage = String (DataUsage.getDataUsage().wifiReceived / 1024 / 1024 )
            
            defaults.set(npUsage, forKey: "npUsage")
        }

        if let usedData2:String = defaults?.object(forKey: "usedData") as? String
        {
            usedData = usedData2
        }
        
       currentUsage = String( (DataUsage.getDataUsage().wifiReceived / 1024 / 1024) - UInt32(npUsage)! )
        
         totalUsedData = String( UInt32(currentUsage)! + UInt32 (usedData)! )
        
        if let packageSize2:String = defaults?.object(forKey: "packageSize") as? String
        {
            packageSize = packageSize2
        }
        
        percent = String((Double(totalUsedData)! * 100) / 1000) + "%"

        updateData()
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
    func updateData() {
        
        myLabel.text = totalUsedData  + "MB" + " / " + packageSize + "MB"
        percentLbl.text = percent + " used"
    }
    
}
